'
' VBA code and functions to support the Learner Logbook process
' The purpose of the code in this file is determine business rule outcomes
' for highligted trips on the ReviewTrips tab of the Learner Logbook
' Assessment Workbook and either validate or invalidate those trips.
' Author: Matthew Hodgson (matt.hodgson@blackbook.ai)
' Date: 12 September 2018
'
'Option Explicit

Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Function AutoCloseMsgBoxFunction(functionName As String, windowName As String)
'Opens a second instance of Excel to allow a second macro to run in sync
'The purpose of the macros in this second instance of Excel are to close dialog window
'which would otherwise pause VBA code execution in the main workbook
     Dim temporaryExcel As Excel.Application
     Set temporaryExcel = OpenAutoCloseWorkbook(windowName)
     Dim x As Integer
     Do
          x = x + 1
          DoEvents
     Loop Until x = 10
     Application.Run functionName 'This is the name of the macro you want to call
     temporaryExcel.Quit
End Function

Function FinalAutoCloseMsgBoxFunction()
'Opens a second instance of Excel to allow a second macro to run in sync
'The purpose of the macros in this second instance of Excel are to close dialog window
'which would otherwise pause VBA code execution in the main workbook
     Dim temporaryExcel As Excel.Application
     Set temporaryExcel = OpenAutoCloseWorkbook("Done")
     Worksheets("INFO PAGE").FinaliseBtn_Click
     Dim x As Integer
     Do
          x = x + 1
          DoEvents
     Loop Until x = 10
     temporaryExcel.Quit
End Function

Function OpenAutoCloseWorkbook(windowName As String) As Excel.Application
'Opens a second instance of Excel to allow a second macro to run in sync
'The purpose of the macros in this second instance of Excel are to close dialog window
'which would otherwise pause VBA code execution in the main workbook
     
     'The primary (current) instance of Excel
     Dim xlApp1 As Excel.Application
     Set xlApp1 = GetObject(, "Excel.Application")
     
     'The second temporary instance of Excel
     Dim xlApp2 As Excel.Application
     Set xlApp2 = CreateObject("Excel.Application")
     xlApp2.Visible = False
     Debug.Print "New Excel instance opened"

     'Open the workbook containing the auto close macro in the second instance of
     Dim FileName As String
     FileName = "C:\SharedData\UiPath\Learner Logbook\VBA\AutoCloseMsgBoxWB.xlsm"
     On Error Resume Next
     Dim xlWB As Workbook
     Debug.Print "Opening new instance of Excel"
     Set xlWB = xlApp2.Workbooks.Open(FileName, True, True)
     Debug.Print "Workbook opened"
     Set OpenAutoCloseWorkbook = xlApp2
     
     'Trigger the MsgBoxProc macro in the secondary instance of Excel
     'The macro continously loops to close VBA triggered msgbox dialog windows
     Debug.Print "Executing secondary workbook macro"
     xlApp2.Run "MsgBoxProc" & windowName
     Debug.Print "Secondary workbook macro running successfully..."

End Function

Function Main() As String
'The main function to evaluate trips in the Learner Logbook
'EvaluateInvalidDates - Reviews the Driver History table retrieved from TICA and inserts
'any invalid date periods into the Invalid Dates sheet
'EvaluateTrips - Loops through the rows on the ReviewTrips sheet and evaluates each row
'against the Business Rules stored in the Config_BusinessRules file
'ApproveLogbook - Approves the log book according to the existing approval macros in the spreadsheet
'This step does involve opening a second workbook with macros focused on clicking and closing the dialog
'windows.  This is required because the existing spreadsheet macros call dialog windows which pause the
'execution of this code, so we run a second instance of Excel to run code to close those windows.
'Returns the assessment result either Passed, Incomplete or an Error Message if the process fails.

     'Workbook variables
     Dim mainWB As Workbook
     Dim xlApp As Excel.Application
     Set xlApp = GetObject(, "Excel.Application")
     Dim mainSht As Worksheet
     Dim assessmentResult As String
     Dim crn As String
     Dim fivePercentRule As String
     Dim errorEncountered As String
     Set mainWB = ActiveWorkbook
     Set mainSht = mainWB.Worksheets("INFO PAGE")
     crn = mainSht.Range("F9").Value

     'Create the assessment log file
     Dim logFile As Object
     Set logFile = CreateLogFile(crn)

     'If there are not enough hours in total, don't evaluate the Logbook at all  
     Dim nightMins As Long
     Dim totalMins As Long
     nightMins = mainSht.Range("I8").Value
     totalMins = mainSht.Range("I10").Value
     If nightMins < 600 Or totalMins < 5700 Then
          assessmentResult = "Not enough hours"
          GoTo EndProcess
     End If

     'Run the EvaluateInvalidDates function
     errorEncountered = EvaluateInvalidDates(crn)
     If Not errorEncountered = "" Then
          GoTo ErrorHandling
     End If
     
	call UpdateInvalidDatesTake2()

     'Run the EvaluateTrips function
     errorEncountered = EvaluateTrips(logFile)
     If Not errorEncountered = "" Then
          GoTo ErrorHandling
     End If
     logFile.Close

     'Return the status of the logbook
     fivePercentRule = mainSht.Range("J23").Value
     If fivePercentRule = "" Then
          assessmentResult = mainSht.Range("J21").Value
     Else
          assessmentResult = fivePercentRule
     End If

     'Call function to remove MsgBox code from existing VBA modules
     Call ReplaceMsgBoxInCodeModule()

     'If approved, run approval macro save copy of the assessed logbook
     If assessmentResult = "Passed" Then
          'Run the ApproveLogbook function
          errorEncountered = ApproveLogbook()
          If Not errorEncountered = "" Then
               GoTo ErrorHandling
          End If
          Application.DisplayAlerts = False
          On Error GoTo ErrorHandling
          'Save a copy of the assessed logbook
          mainWB.SaveAs "C:\SharedData\UiPath\Learner Logbook\VBA\Passed\" & crn & " - Passed"
          Application.DisplayAlerts = True
     Else
          'Save a copy of the assessed logbook for review purposes
          Application.DisplayAlerts = False
          On Error GoTo ErrorHandling
          'Call function to remove MsgBox code from existing VBA modules
          mainWB.SaveAs "C:\SharedData\UiPath\Learner Logbook\VBA\Incomplete\" & crn & " - Incomplete"
          Application.DisplayAlerts = True
     End If

EndProcess:
     'Return the assessed logbook result
     Debug.Print "Logbook processed: " & assessmentResult & vbNewLine & "End Process"
     Main = assessmentResult
     logFile.Close
     Exit Function

ErrorHandling:
     'In case of an error, return the error message and reenable all screen updating and alerts
     Debug.Print "An unrecoverable error was encountered when processing the  logbook: " & errorEncountered
     Main = errorEncountered
     Application.ScreenUpdating = True
     Application.DisplayAlerts = True
     Application.EnableEvents = True
End Function

Function ApproveLogbook() As String
'Approves the logbook by click the existing macro Approve button

     'Main workbook variables
     Dim mainSht As Worksheet
     Set mainSht = Worksheets("INFO PAGE")
     Dim thisWorkbookName As String
     thisWorkbookName = Application.Caption

     'Click the APPROVE button
     Worksheets("INFO PAGE").ApprovedBtn_Click

     'Click the PRINT FILING button (just calls the relevant macro)
     Call AutoCloseMsgBoxFunction("PrintFilingMacro", "Print")
     
     'Send the SMS or Email message
     Dim preferredContactMethod As String
     preferredContactMethod = mainSht.Range("F13").Value
     
     'Call the SMS or Email function depending on the learner's preference
     If preferredContactMethod = "SMS" Then
          Call SendSMS
     Else
          Call SendEmail
     End If
      
     'Close the Have you approved logbook in TICA dialog
     'Please see commentary
     Call FinalAutoCloseMsgBoxFunction

ErrorHandling:
     'Print the error and return it to the calling function
     Debug.Print "Unrecoverable error in function ApproveLogbook: " & Err.Description
     ApproveLogbook = Err.Description
End Function

Function SendSMS()
'Copy of existing SendSMS function
'Only change is the addition of an extra .Body = replace line to insert the robot's initials
     Dim outlookOBJ As Outlook.Application
     Dim mItem As Outlook.MailItem
     Set outlookOBJ = New Outlook.Application
     Set mItem = outlookOBJ.CreateItemFromTemplate(Worksheets("Supervisor Details").Range("J57"))
     With mItem
          .To = Worksheets("INFO PAGE").Range("F12") & "@e2s.pcsms.com.au"
          .SentOnBehalfOfName = "Learner.Logbook.CPU@tmr.qld.gov.au"
          .Body = Replace(.Body, "<Name>", Worksheets("INFO PAGE").Range("F7").Value)
          .Body = Replace(.Body, "{end}", "{end} RPA")
          .Send
     End With
     Sheets("Info Page").Unprotect
          Dim LogRow As Long
          LogRow = Sheets("Info Page").Range("O1000").End(xlUp).row + 1
          Sheets("Info Page").Range("O" & LogRow) = "SMS Generated - Logbook Approval"
     Sheets("Info Page").Protect
End Function

Function SendEmail()
'Copy of existing SendEmail function
'Only change is the addition of an extra .Body = replace line to insert the robot's name into the email
     Dim outlookOBJ As Outlook.Application
     Dim mItem As Outlook.MailItem
     Set outlookOBJ = New Outlook.Application
     Set mItem = outlookOBJ.CreateItemFromTemplate(Worksheets("Supervisor Details").Range("H57"))
     With mItem
          .To = Worksheets("INFO PAGE").Range("F11")
          .Body = Replace(.Body, "<Insert Name>", Worksheets("INFO PAGE").Range("F7").Value)
          .Body = Replace(.Body, "Regards", "Regards, " & vbCrLf & vbCrLf & "RPA")
          .SentOnBehalfOfName = "Learner.Logbook.CPU@tmr.qld.gov.au"
          .Send
     End With
     Sheets("Info Page").Unprotect
          Dim LogRow As Long
          LogRow = Sheets("Info Page").Range("O1000").End(xlUp).row + 1
          Sheets("Info Page").Range("O" & LogRow) = "Email Generated - Logbook Approval"
     Sheets("Info Page").Protect
End Function

Function EvaluateInvalidDates(crn As String) As String
'Loop through the driver history table, retrieve invalid codes and calculate invalid periods
'Insert these dates into the template worksheet
'...add commentary around why the entire process loops twice... future matt please help

     'Disable screen updating for performance
     Application.ScreenUpdating = False
     
     'Main workbook variables
     Dim mainWB As Workbook
     Set mainWB = ActiveWorkbook
     Dim mainSht As Worksheet

     'Driver history workbook variables
     Dim historyWB As Workbook
     Dim historySht As Worksheet
     Dim headerRow As Range
     Dim find As Range
     Dim col_Action As Integer
     Dim col_SystemDate As Integer
     Dim col_ActionDate As Integer
     Dim col_Description As Integer
     
     'Open the Driver History Datatable csv file
     Debug.Print "Opening Driver History temp file"
     On Error GoTo UnableToOpenDriverHistoryWorkbook
     Set historyWB = Workbooks.Open("C:\SharedData\UiPath\Learner Logbook\VBA\Temp\Driver History Datatable.csv")
     Debug.Print "Driver History temp file opened"
     Set historySht = historyWB.Sheets("Driver History Datatable")
     Set headerRow = historySht.Rows(1)
     
     'Set columns to be used
     Debug.Print "Determining column headers"
     On Error GoTo UnableToFindHeaders
     col_Action = headerRow.find(What:="Action", LookAt:=xlWhole, MatchCase:=False, SearchFormat:=False).Column
     col_SystemDate = headerRow.find(What:="System Date", LookAt:=xlWhole, MatchCase:=False, SearchFormat:=False).Column
     col_ActionDate = headerRow.find(What:="Action Date", LookAt:=xlWhole, MatchCase:=False, SearchFormat:=False).Column
     col_Description = headerRow.find(What:="Description", LookAt:=xlWhole, MatchCase:=False, SearchFormat:=False).Column
               
     'Get first and last row numbers, we'll do the loop in reverse
     Dim currentRow As Integer
     Dim lastRow As Integer
     Dim topRow As Integer
     topRow = headerRow.Row + 1
     currentRow = historySht.UsedRange.Rows.Count
     lastRow = historySht.UsedRange.Rows.Count
     
     Dim sanc_SperSusApplied As String
     Dim sanc_SperSusLifted As String
     Dim sanc_QldSusApplied As String
     Dim sanc_QldSusExpired As String
     Dim sanc_DemeritSuspension As String

     sanc_SperSusApplied = "SPER SUSPENSION APPLIED"
     sanc_SperSusLifted = "SPER SUSPENSION LIFTED"
     sanc_QldSusApplied = "IMMEDIATE SUSPENSION - QLD LICENCE APPLIED"
     sanc_QldSusExpired = "IMMEDIATE SUSPENSION - QLD LICENCE EXPIRED"
     sanc_DemeritSuspension = "DEMERIT POINT SUSPENSION"
     
     Dim firstLoop As Boolean
     firstLoop = True
     Dim processedFirstAddDate As Boolean
     Dim findAddRenwGaps As Boolean
     processedFirstAddDate = False
     
     Dim gapRow As Integer
     gapRow = lastRow + 1
     
LoopAgain:
     
     'Loop through the driver history workbook from bottom to top and insert effective and expiry dates
     Do While currentRow >= topRow
          
          'Retrieve Action code
          Dim val_Action As String
          Dim val_Description As String
          val_Action = Cells(currentRow, col_Action)
          val_Description = Cells(currentRow, col_Description).Value
          
          'Range to insert effective and expiry dates
          Dim effDate As Range
          Dim expDate As Range
          Set effDate = historySht.Cells(currentRow, col_Description + 1)
          Set expDate = historySht.Cells(currentRow, col_Description + 2)
          
          'Ranges to insert gap eff and exp date
          Dim gapEffDate As Range
          Dim gapExpDate As Range
          Dim gapAction As Range
          Set gapAction = historySht.Cells(gapRow, 1)
          Set gapEffDate = historySht.Cells(gapRow, col_Description + 1)
          Set gapExpDate = historySht.Cells(gapRow, col_Description + 2)
          
          Dim startDates As Range
          Dim endDates As Range
          Dim descriptions As Range
          Dim expVlookupRange As Range
          Dim retrievedDate As Date
          Dim firstAddExpDate As Date
          Dim lastGapExpDate As Date
          
          Select Case val_Action
               
               Case "ADD", "REIS", "RENW"
                    Debug.Print "Processing ADD, REIS and RENW dates"
                    
                    'Effective date is 12am on the EFF date
                    'Expiry date is 12am on the next day after EXP date
                    effDate.Value = GetEffectiveDate(val_Description)
                    expDate.Value = GetExpiryDate(val_Description) + 1
                    
                    'Detect the first ADD row
                    If val_Action = "ADD" And processedFirstAddDate = False Then
                        Debug.Print "Found the first ADD row"
                        firstAddExpDate = GetExpiryDate(val_Description) + 1
                        lastGapExpDate = firstAddExpDate
                        processedFirstAddDate = True
                        
                        If firstAddExpDate > Now() Then
                            'No issues
                            Debug.Print "No gaps in ADD/RENW dates"
                            findAddRenwGaps = False
                            Else
                            'Find gaps in ADD/RENW dates
                            Debug.Print "There were gaps in the ADD/RENW dates"
                            findAddRenwGaps = True
                        End If
                    End If
                    
                    'If you then come across a RENW record, check if there's a gap between the two
                    If findAddRenwGaps = True And val_Action = "RENW" Then
                        Debug.Print "Found a RENW date"
                        Dim latestEffDate As Date
                        Dim latestEXPDate As Date
                        latestEffDate = GetEffectiveDate(val_Description)
                        latestEXPDate = GetExpiryDate(val_Description)

                        'Check if there's a gap between the firstAddExpDate and the current RENW EFF date
                        If latestEffDate - lastGapExpDate <= 0 Then
                            'There's no gap
                            Debug.Print "The RENW EFF date is effective from the last EXP date - OK "
                            findAddRenwGaps = False
                        
                        Else
                            'Get the gap dates
                            Dim gapStartDate As Date
                            Dim gapEndDate As Date
                            gapStartDate = lastGapExpDate
                            gapEndDate = latestEffDate - 1
                            lastGapExpDate = latestEXPDate
                            
                            'Insert a new RPA1 row with the eff and exp dates
                            gapAction.Value = "RPA1"
                            gapEffDate.Value = gapStartDate
                            gapExpDate.Value = gapEndDate
                            gapRow = gapRow + 1
                        End If
                    End If
                        
               Case "CANC"
                    Debug.Print "Processing CANC dates"
                    
                    'Effective date is the EFF date
                    'Expiry date is the following ADD/REIS EFF date, else today's date
                    effDate.Value = GetEffectiveDate(val_Description)
                    
                    'The Vlookup range is the Actions column to the Effective Date column
                    Set expVlookupRange = Range(Cells(topRow, col_Action), Cells(currentRow, col_Description + 2))
                    On Error Resume Next
                    
                    'Attempt to retrieve the ADD EFF date
                    retrievedDate = 0
                    retrievedDate = WorksheetFunction.VLookup("ADD", expVlookupRange, 5, False)
                    
                    'If that's unsuccessful, attempt to retrieve the REIS EFF date
                    If retrievedDate = 0 Then
                         retrievedDate = WorksheetFunction.VLookup("REIS", expVlookupRange, 5, False)
                    End If
                    
                    'If that is also unsuccesful, set the retrieved date as today's date (as no trips should have occurred in the future)
                    If retrievedDate = 0 Then
                         retrievedDate = Date
                    End If

                    'Set the final EXP date as the retrieved date
                    expDate.Value = retrievedDate
                    
               Case "SANC"
                    Debug.Print "Processing SANC dates"
                    
                    'For SANC codes, check if they're SPER suspensions, QLD Licence Suspensions or Demerit Pt suspensions
                    Select Case True
                    
                         'SPER Suspensions
                         Case InStr(val_Description, sanc_SperSusApplied), InStr(val_Description, sanc_SperSusLifted)
                              Debug.Print "Processing SPER suspensions dates"

                              'If SPER Suspension Applied, effective date is the EFF date
                              If InStr(val_Description, sanc_SperSusApplied) Then
                                   effDate.Value = GetEffectiveDate(val_Description)
                                   Set expVlookupRange = Range(Cells(topRow, col_Description), Cells(currentRow, col_Description + 2))
                                   
                                   'Attempt to retrieve the EFF date
                                   On Error Resume Next
                                   retrievedDate = 0
                                   retrievedDate = WorksheetFunction.VLookup(sanc_SperSusLifted & "*", expVlookupRange, 3, False)
                                   expDate.Value = retrievedDate
                              
                              'If SPER Suspension lifted, the expiry date is the EFF date
                              Else
                                   expDate.Value = GetEffectiveDate(val_Description)
                                   Set startDates = Range(Cells(currentRow, effDate.Column), Cells(lastRow, effDate.Column))
                                   Set descriptions = Range(Cells(currentRow, col_Description), Cells(lastRow, col_Description))
                                   
                                   'Attempt to retrieve the ADD EFF date
                                   On Error Resume Next
                                   retrievedDate = 0
                                   retrievedDate = WorksheetFunction.Index(startDates, WorksheetFunction.Match(sanc_SperSusApplied & "*", descriptions, 0), 0)
                                   
                                   'If retrieved date is nothing, then use the current date
                                   If retrievedDate = 0 Then
                                        effDate.Value = Date
                                   Else
                                        'Use the retrieved date
                                        effDate.Value = retrievedDate
                                   End If
                              End If
                         
                         'Demerit Point Suspensions
                         Case InStr(val_Description, sanc_DemeritSuspension)
                              Debug.Print "Processing demerit point suspensions"

                              'For demerit point suspensions, retrieve the EFF and EXP date from the description
                              effDate.Value = GetEffectiveDate(val_Description)
                              expDate.Value = GetExpiryDate(val_Description) + 1
                         
                         'QLD Licence Suspensions
                         Case InStr(val_Description, sanc_QldSusApplied), InStr(val_Description, sanc_QldSusExpired)
                              Debug.Print "Processing QLD licence suspensions"

                              'If suspension Applied, effective date is the EFF date
                              If InStr(val_Description, sanc_QldSusApplied) Then
                                   effDate.Value = GetEffectiveDate(val_Description)
                                   Set expVlookupRange = Range(Cells(topRow, col_Description), Cells(currentRow, col_Description + 2))
                                   On Error Resume Next
                                   retrievedDate = WorksheetFunction.VLookup(sanc_QldSusExpired & "*", expVlookupRange, 3, False)
                                   expDate.Value = retrievedDate
                                   
                              'Otherwise, if suspension expired, the expiry date is the EFF date
                              Else
                                   expDate.Value = GetEffectiveDate(val_Description)
                                   Set startDates = Range(Cells(currentRow, effDate.Column), Cells(lastRow, effDate.Column))
                                   Set descriptions = Range(Cells(currentRow, col_Description), Cells(lastRow, col_Description))
                                   
                                   'Attempt to retrieve the ADD EFF date
                                   On Error Resume Next
                                   retrievedDate = 0
                                   retrievedDate = WorksheetFunction.Index(startDates, WorksheetFunction.Match(sanc_QldSusApplied & "*", descriptions, 0), 0)
                                   
                                   'If retrieved date is nothing, then use the current date
                                   If retrievedDate = 0 Then
                                        effDate.Value = Date
                                   Else
                                        'Use the retrieved date
                                        effDate.Value = retrievedDate
                                   End If
                              End If
                    End Select
          End Select

          'Increment the row counter
          currentRow = currentRow - 1
     Loop
     
     'Reset the current row counter to the last Row number so we can loop again
     currentRow = lastRow
     'Checks if the first loop has completed and if so, completes it one more time to match
     'EXP and EFF dates for records that span across multiple rows
     GoTo CheckIfRepeatLoop

'If the second has already been completed, 'CheckIfRepeatLoop' will return here
BackToEnd:

'    Dim veryLastExpDate As Date
'    Dim veryLastExpAction As String
'    veryLastExpAction = historySht.Cells(gapRow - 1, 1).Value
'
'    If veryLastExpAction = "RPA1" Then
'        veryLastExpDate = historySht.Cells(gapRow - 1, 6).Value
'        If veryLastExpDate < Now() Then
'            historySht.Cells(gapRow, 1).Value = "RPA1"
'            historySht.Cells(gapRow, 5).Value = veryLastExpDate
'            historySht.Cells(gapRow, 6).Value = Format(Now, "dd/mm/yyyy")
'        End If
'    End If
    
    Dim latestRENWDate As Date
    Dim latestADDDate As Date
    Dim finalEXPDate As Date

    On Error Resume Next
    latestRENWDate = Application.WorksheetFunction.VLookup("RENW", historySht.Range("A:F"), 6, False)
    latestADDDate = Application.WorksheetFunction.VLookup("ADD", historySht.Range("A:F"), 6, False)
    If latestRENWDate > latestADDDate Then
        finalEXPDate = latestRENWDate
    Else
        finalEXPDate = latestADDDate
    End If
    
    If finalEXPDate < Now Then
        historySht.Cells(gapRow, 1).Value = "RPA1"
        historySht.Cells(gapRow, 5).Value = finalEXPDate
        historySht.Cells(gapRow, 6).Value = Date
    End If
    
     'Update the Invalid Dates sheet
     Dim invalidDatesSht As Worksheet
     Set invalidDatesSht = mainWB.Sheets("Invalid Dates")
     Dim mainStartDate As Range
     Dim mainEndDate As Range
     Dim mainRow As Integer
     mainRow = 14
     currentRow = topRow
     
     Do While currentRow <= lastRow

          val_Action = Cells(currentRow, col_Action)
          Set mainStartDate = invalidDatesSht.Cells(mainRow, 3)
          Set mainEndDate = invalidDatesSht.Cells(mainRow, 4)
               
          If val_Action = "SANC" Or val_Action = "CANC" Or val_Action = "RPA1" Then
               mainStartDate = historySht.Cells(currentRow, col_Description + 1)
               mainEndDate = historySht.Cells(currentRow, col_Description + 2)
               mainRow = mainRow + 1
          Else
                  'Do nothing
          End If
          
          currentRow = currentRow + 1
     Loop

     'Reenable screen updating
     Application.ScreenUpdating = True

     Debug.Print "Driver history data processed"
     'Save a copy of the processed Drivery History workbook
     Application.DisplayAlerts = False
     historyWB.SaveAs "C:\SharedData\UiPath\Learner Logbook\VBA\Driver History\" & crn & " - Driver History.csv"
     historyWB.Close SaveChanges:=False
     Application.DisplayAlerts = True
     Debug.Print "Temporary driver history workbook closed"
     Exit Function
     
     'Error Handling
UnableToFindHeaders:
     'Reenable screen updating and return error message
     Application.ScreenUpdating = True
     EvaluateInvalidDates = "Error in Function EvaluateInvalidDates: Unable to find header row in the driver history datatable? " & Err.Description
     Debug.Print "Error in EvaluateInvalidDates: Unable to find header row in the driver history datatable.  Are the headers on Row 1? " & Err.Description
     Exit Function
     
UnableToOpenDriverHistoryWorkbook:
     'Reenable screen updating and return error message
     Application.ScreenUpdating = True
     EvaluateInvalidDates = "Error in Function EvaluateInvalidDates: Unable to open the driver history workbook. " & Err.Description
     Debug.Print "Unable to open the driver history workbook.  Has it been created?" & Err.Description
     Exit Function
     
CheckIfRepeatLoop:
     'Checks if the main loop has already been completed and executes it a second time if so
     'Otherwise, returns code to complete the Function
     If firstLoop = True Then
          firstLoop = False
          Debug.Print "Looping through sheet again to match entered start/end dates"
          GoTo LoopAgain
     Else
          GoTo BackToEnd
     End If
End Function

Function GetEffectiveDate(val_Description As String) As Date
     
     Dim splitString() As String
     'Get the effective date of the current row
     splitString = Split(val_Description, "EFF ", -1)
     On Error GoTo notFound
     If Not splitString(1) = "" Then
          'Retrieve the date value
          splitString = Split(splitString(1), ",", -1)
          GetEffectiveDate = splitString(0)
     Else
notFound:
          'No effective date found in the description
          GetEffectiveDate = 0
     End If

End Function

Function GetExpiryDate(val_Description As String) As Date

     Dim splitString() As String
     'Get the effective date of the current row
     splitString = Split(val_Description, "EXP ", -1)
     On Error GoTo notFound
     If Not splitString(1) = "" Then
          'Retrieve the date value
          splitString = Split(splitString(1), ",", -1)
          GetExpiryDate = splitString(0)
     Else
notFound:
          'No effective date found in the description
          GetExpiryDate = 0
     End If

End Function

Function EvaluateTrips(logFile As Object) As String
'Loops through the trips on the ReviewTrips sheet and validates them according to the business
'rules specified in the business rules configuration file
     
     'Disable screen updating for speed
     Application.ScreenUpdating = False
     On Error GoTo Cleanup:
     
     'Setup the main workbook variables
     Dim mainWB As Workbook
     Dim reviewTripsSht As Worksheet
     Dim rawDataSht As Worksheet
     Dim row As Integer
     Dim lastRow As Integer
     Set mainWB = ActiveWorkbook
     Set reviewTripsSht = mainWB.Sheets("ReviewTrips")
     Set rawDataSht = mainWB.Sheets("Data")
     mainWB.Sheets("ReviewTrips").Unprotect
     mainWB.Sheets("Data").Unprotect
     mainWB.Sheets("ReviewTrips").Activate
     
     'Setup the config workbook variables
     Dim configWB As Workbook
     Debug.Print "Opening business rules workbook"
     Set configWB = Workbooks.Open("C:\SharedData\UiPath\Learner Logbook\VBA\Config_BusinessRules.xlsx")
     Dim brSht As Worksheet
     Dim locSht As Worksheet
     Set brSht = configWB.Sheets("BusinessRules")
     Set locSht = configWB.Sheets("InvalidLocations")
     Dim brShtRw As Integer
     Dim brShtRws As Integer
     Dim locShtRw As Integer
     Dim locShtRws As Integer
     brShtRws = brSht.UsedRange.Rows.Count
     brShtRw = 2
     locShtRws = locSht.UsedRange.Rows.Count
     locShtRw = 1
     'Build the arrays to hold the config file values
     Dim businessRules() As String
     Dim invalidLocations() As String
     locShtRws = locShtRws - 1 'remove one for the headers
     ReDim invalidLocations(locShtRws - 1, 0)
     brShtRws = brShtRws - 2 ' remove 2 for headers and 0 array start
     ReDim businessRules(brShtRws, 7) '-1 for array start at 0
     'businessRules array structure
     '[0]           [1]     [2]       [3]             [4]         [5]        [6]           [7]
     '[BusinessRule][Column][Operator][Criteria/Value][Exceptions][Tolerance][ErrorMessage][Evaluation]
     'Loop through the business rules sheet and add values to the businessRules array
     Debug.Print "Building array to hold business rules"
     Dim counter As Integer
     counter = 0
     Do While counter <= brShtRws
          businessRules(counter, 0) = brSht.Range("A" & brShtRw).Value
          businessRules(counter, 1) = brSht.Range("B" & brShtRw).Value
          businessRules(counter, 2) = brSht.Range("C" & brShtRw).Value
          businessRules(counter, 3) = brSht.Range("D" & brShtRw).Value
          businessRules(counter, 4) = brSht.Range("E" & brShtRw).Value
          businessRules(counter, 5) = brSht.Range("F" & brShtRw).Value
          businessRules(counter, 6) = brSht.Range("G" & brShtRw).Value
          businessRules(counter, 7) = "False"
          counter = counter + 1
          brShtRw = brShtRw + 1
     Loop

     'Loop through the invalid locaions sheet and add values to the invalide locations array
     Debug.Print "Building array to hold invalid locations"
     Do While locShtRw <= locShtRws
          invalidLocations(locShtRw - 1, 0) = locSht.Range("A" & locShtRw + 1).Value
          locShtRw = locShtRw + 1
     Loop

     Debug.Print "Closing business rules workbook"
     configWB.Close SaveChanges:=False 'Close the config workbook
     Debug.Print "Business rules workbook closed"

     'Variables to hold the trip values
     Dim startLocation As String
     Dim finishLocation As String
     Dim startOdo As String
     Dim finishOdo As String
     Dim startTime As String
     Dim finishTime As String
     Dim tripDuration As String
     Dim dayMins As String
     Dim nightMins As String
     Dim accreditedMins As String
     Dim estSpeed As String
     Dim drivingSchool As String
     Dim rego As String
     Dim tripDate As String
     Dim pastOrRealTime As String
     Dim adjustTripStatus As String
     Dim validTrips As Long
     Dim invalidTrips As Long
     Dim percentageOfValidTrips As Double
     Dim licenceIssueDate As Date
     Dim tripStatus As String

     'Import the progress bar if necessary, skip if already loaded
     On Error GoTo ProgressBarLoaded
     Debug.Print "Importing progress bar userform"
     Application.VBE.ActiveVBProject.VBComponents.Import "C:\SharedData\UiPath\Learner Logbook\VBA\ProgressBar.frm"
ProgressBarLoaded:
     Debug.Print "Successfully imported progress bar userform"
     'Display the progress bar
     Sleep 500
     Call DisplayProgressBar

     'Loop through each row in the ReviewTrips sheet
     row = 2
     lastRow = WorksheetFunction.CountIf(reviewTripsSht.Range("B:B"), ">0") + 1
     '--------- uncomment above / delete below
     Debug.Print "Start looping through the rows on the ReviewTrips sheet"
     Do While row <= lastRow
          Dim invalidTrip As String
          Dim prevBrResult As String
          invalidTrip = ""
          'Get trip details for the current row in the ReviewTrips sheet
          licenceIssueDate = Sheets("Invalid Dates").Range("D6").Value
          startLocation = Trim(UCase(Cells(row, WorksheetFunction.Match("Start location", Range("1:1"), 0)).Value))
          finishLocation = Trim(UCase(Cells(row, WorksheetFunction.Match("Finish location", Range("1:1"), 0)).Value))
          estSpeed = Cells(row, WorksheetFunction.Match("Estd Speed (km/hr)", Range("1:1"), 0)).Value
          startOdo = Cells(row, WorksheetFunction.Match("Start odometer", Range("1:1"), 0)).Value
          finishOdo = Cells(row, WorksheetFunction.Match("Finish odometer", Range("1:1"), 0)).Value
          startTime = Format(Cells(row, WorksheetFunction.Match("Trip start time", Range("1:1"), 0)).Value, "hh:mm am/pm")
          finishTime = Format(Cells(row, WorksheetFunction.Match("Trip finish time", Range("1:1"), 0)).Value, "hh:mm am/pm")
          dayMins = Cells(row, WorksheetFunction.Match("Day mins", Range("1:1"), 0)).Value
          nightMins = Cells(row, WorksheetFunction.Match("Night mins", Range("1:1"), 0)).Value
          rego = Cells(row, WorksheetFunction.Match("Rego #", Range("1:1"), 0)).Value
          tripDate = Format(Cells(row, WorksheetFunction.Match("Date of trip", Range("1:1"), 0)).Value, "dd/mm/yyyy")
          tripDuration = Cells(row, WorksheetFunction.Match("Driven mins", Range("1:1"), 0)).Value
          accreditedMins = Cells(row, WorksheetFunction.Match("Accredited DT Mins", Range("1:1"), 0)).Value
          drivingSchool = Cells(row, WorksheetFunction.Match("Driving school", Range("1:1"), 0)).Value
          pastOrRealTime = Cells(row, WorksheetFunction.Match("Past/real time", Range("1:1"), 0)).Value
          adjustTripStatus = Cells(row, WorksheetFunction.Match("Adjust Trip Status", Range("1:1"), 0)).Value
          
          'Retrieve details from the Data sheet (existing spreadsheet formulas)
          Dim odometerDiscrepancyOk As Boolean
          Dim macroResult As String
          Dim suburb1 As String
          Dim suburb2 As String
          Dim odometer1 As String
          Dim odometer2 As String
          Dim helperColumn As String
          Dim helperColumnNo As Integer
          Dim prevTripStartSuburb As String
          Dim prevTripEndSuburb As String
          suburb1 = Trim(UCase(Sheets("Data").Cells(row, WorksheetFunction.Match("Start location", Sheets("Data").Range("1:1"), 0)).Value))
          suburb2 = Trim(UCase(Sheets("Data").Cells(row, WorksheetFunction.Match("Finish suburb of last trip by this LPN", Sheets("Data").Range("1:1"), 0)).Value))
          suburb1 = AlphaNumericOnly(suburb1)
          suburb2 = AlphaNumericOnly(suburb2)
          odometer1 = Sheets("Data").Cells(row, WorksheetFunction.Match("Start odometer", Sheets("Data").Range("1:1"), 0)).Value
          odometer2 = Sheets("Data").Cells(row, WorksheetFunction.Match("Odo Reading at end of last trip", Sheets("Data").Range("1:1"), 0)).Value
          macroResult = Sheets("Data").Cells(row, 27).Value
          helperColumn = Trim(UCase(Sheets("Data").Cells(row, WorksheetFunction.Match("HelperColumn", Sheets("Data").Range("1:1"), 0)).Value))
          helperColumnNo = GetNumeric(Sheets("Data").Cells(row, WorksheetFunction.Match("Times LPN has appeared in list so far", Sheets("Data").Range("1:1"), 0)).Value)
          If helperColumnNo = 1 Or helperColumnNo = 0 Then
            prevTripStartSuburb = "Not applicable"
            prevTripEndSuburb = "Not applicable"
          Else
            Dim col_HelperColumn As Integer
            Dim col_StartSuburbs As Integer
            col_HelperColumn = WorksheetFunction.Match("HelperColumn", Sheets("Data").Range("1:1"), 0)
            col_StartSuburbs = WorksheetFunction.Match("Start location", Sheets("Data").Range("1:1"), 0)
            col_EndSuburbs = WorksheetFunction.Match("Finish location", Sheets("Data").Range("1:1"), 0)
            helperColumnNo = helperColumnNo - 1
            helperColumn = rego & helperColumnNo
            prevTripStartSuburb = Trim(UCase(WorksheetFunction.Index(Sheets("Data").Range("C:C"), WorksheetFunction.Match(helperColumn, Sheets("Data").Range("W:W"), 0), 1)))
            prevTripEndSuburb = Trim(UCase(WorksheetFunction.Index(Sheets("Data").Range("D:D"), WorksheetFunction.Match(helperColumn, Sheets("Data").Range("W:W"), 0), 1)))
            prevTripStartOdo = Trim(UCase(WorksheetFunction.Index(Sheets("Data").Range("E:E"), WorksheetFunction.Match(helperColumn, Sheets("Data").Range("W:W"), 0), 1)))
            prevTripEndOdo = Trim(UCase(WorksheetFunction.Index(Sheets("Data").Range("F:F"), WorksheetFunction.Match(helperColumn, Sheets("Data").Range("W:W"), 0), 1)))
            Dim distanceBetweenPrevLocations As String
            distanceBetweenPrevLocations = prevTripEndOdo - prevTripStartOdo
          End If

          'Print current row trip information
          Debug.Print vbCrLf & "Trip " & row - 1 & " - Date: " & tripDate & " " & startLocation & " - " & finishLocation & ", Odometer: " & startOdo & " - " & finishOdo & ", Time: " & startTime & " - " & finishTime & ", Day mins: " & dayMins & ", Night mins: " & nightMins & ", Rego: " & rego & ", Duration: " & tripDuration
          Call WriteToLogFile(logFile, vbCrLf & "Trip " & row - 1 & " - Date: " & tripDate & " " & startLocation & " - " & finishLocation & ", Odometer: " & startOdo & " - " & finishOdo & ", Time: " & startTime & " - " & finishTime & ", Day mins: " & dayMins & ", Night mins: " & nightMins & ", Rego: " & rego & ", Duration: " & tripDuration)
          
         if adjustTripStatus = "Invalid learner" then
         	tripStatus = "Invalid learner"
         	goto invalidLocationConfirmed
         end if

         if adjustTripStatus = "Prior to licence issue" then
			tripStatus = "Prior to licence issue"
         	goto invalidLocationConfirmed
         end if

          'Validate the current row against the business rules
          Dim brRow As Integer
          Dim locRow As Integer
          Dim brName As String
          Dim brColumn As String
          Dim brOperand As String
          Dim brErrorMessage As String
          Dim brCriteria As String
          Dim brException As String
          Dim brTolerance As String
          brRow = 0
          locRow = 0
          Dim locName As String

          'Loop through the invalid locations array and invalid the trip if necessary
          'Do this first as it's quick and avoids all the other business rules having to evaluate
          'In Production, it would be recommended to add a business rule around this, and say
          'if there are <5% of trips containing Home/Work/School etc in the entire logbook, then
          'evaluate the other business rules against the trip, and validate it if no issues are found.
          While locRow < locShtRws
               locName = Trim(UCase(invalidLocations(locRow, 0)))
               If startLocation = locName Or finishLocation = locName Then
                    tripStatus = "Invalid"
                    GoTo invalidLocationConfirmed
               End If
               locRow = locRow + 1
          Wend

          'Reset the previous business rule result value
          prevBrResult = ""

          'Loop through each business rule
          While brRow < brShtRws And Not prevBrResult = "True"
               brName = businessRules(brRow, 0)
               brColumn = businessRules(brRow, 1)
               brOperand = businessRules(brRow, 2)
               brCriteria = businessRules(brRow, 3)
               brException = businessRules(brRow, 4)
               brTolerance = businessRules(brRow, 5)
               brErrorMessage = businessRules(brRow, 6)
                    
               '-----------------------------------------------------------
               ' Execute the relevant business rule function for each trip
               '-----------------------------------------------------------

               Select Case brName
                      
                    Case "ExcessiveSpeed"
                         businessRules(brRow, 7) = Evaluate_ExcessiveSpeed(estSpeed, brOperand, brCriteria, brTolerance, brErrorMessage, _
                         startLocation, finishLocation, tripDuration, accreditedMins, drivingSchool, logFile)
                         
                    Case "LowAverageSpeed"
                         businessRules(brRow, 7) = Evaluate_LowAverageSpeed(estSpeed, brOperand, brCriteria, brTolerance, startLocation, _
                         finishLocation, brException, tripDuration, startTime, finishTime, accreditedMins, drivingSchool, brErrorMessage, logFile)
                    
                    Case "PriorToLicenceIssueDate"
                         businessRules(brRow, 7) = Evaluate_PriorToLicenceIssueDate(adjustTripStatus, brErrorMessage, logFile)

                    Case "InvalidLearner"
                         businessRules(brRow, 7) = Evaluate_InvalidLearner(adjustTripStatus, logFile)
                         
                    Case "InvalidSupervisor"
                         businessRules(brRow, 7) = Evaluate_InvalidSupervisor(logFile)
                         'Out of scope for POC - left in for completeness

                    Case "IncorrectTime"
                         businessRules(brRow, 7) = Evaluate_IncorrectTime(startTime, finishTime, brErrorMessage, logFile)
                         
                    Case "IncorrectOdometer"
                         businessRules(brRow, 7) = Evaluate_IncorrectOdometer(startOdo, finishOdo, brCriteria, brErrorMessage, logFile)
                         
                    Case "LocationOdometerMismatch"
                         businessRules(brRow, 7) = Evaluate_LocationOdometerMismatch(macroResult, suburb1, suburb2, odometer1, odometer2, _
                         brErrorMessage, brTolerance, startLocation, prevTripStartSuburb, prevTripEndSuburb, distanceBetweenPrevLocations, logFile)

                    Case "TenHourTrip"
                         businessRules(brRow, 7) = Evaluate_TenHourTrip(tripDuration, startLocation, finishLocation, brTolerance, brErrorMessage, logFile)
                         
               End Select
            
               'Save the business rule evaluation of this trip so it can be checked at the beginning of the next loop
               prevBrResult = businessRules(brRow, 7)
            
               'Increment the row counter
               brRow = brRow + 1
               
          Wend
          
          '---------------------------------------------
          'Determine if the trip is valid based on the outcome of the evaluated business rules
          tripStatus = TripValidity(row, businessRules(), brShtRws)
        
invalidLocationConfirmed:
          'Update the trip record according to the trip being Valid or Invalid
          Call ProcessTripRow(row, tripStatus, logFile)

          'Count the number of valid and invalid trips
          If tripStatus = "Valid" Then
               validTrips = validTrips + 1
          Else
               invalidTrips = invalidTrips + 1
          End If

          'Update the progress bar
          Call UpdateProgressBar(row, lastRow)

          row = row + 1 'increment the row counter
     Loop
     
     'Hide the progress bar
     Call HideProgressBar
     
     'Determine Logbook assessment result
     Debug.Print vbCrLf & "Trips analysed: " & invalidTrips + validTrips
     Debug.Print "Valid trips: " & validTrips
     Debug.Print "Invalid trips: " & invalidTrips

     Call WriteToLogFile(logFile, vbCrLf & "Trips analysed: " & invalidTrips + validTrips)
     Call WriteToLogFile(logFile, "Valid trips: " & validTrips)
     Call WriteToLogFile(logFile, "Invalid trips: " & invalidTrips)

     Dim final_LogbookAppStatus As String
     Dim final_DayMins As Integer
     Dim final_NightMins As Integer
     Dim final_Minutes As Integer
     final_LogbookAppStatus = Sheets("INFO PAGE").Range("J21").Value
     If final_LogbookAppStatus = "Incomplete" Then
        final_LogbookAppStatus = Sheets("INFO PAGE").Range("J23").Value
     End If
     final_DayMins = Sheets("INFO PAGE").Range("K16").Value
     final_NightMins = Sheets("INFO PAGE").Range("K17").Value
     final_Minutes = final_DayMins + final_NightMins
     Debug.Print "Day mins: " & final_DayMins
     Debug.Print "Night mins: " & final_NightMins
     Debug.Print "Total mins: " & final_Minutes

     Call WriteToLogFile(logFile, "Day mins: " & final_DayMins)
     Call WriteToLogFile(logFile, "Night mins: " & final_NightMins)
     Call WriteToLogFile(logFile, "Total mins: " & final_Minutes)
     
     'Confirm final results was a pass (not used in the evaluation of the logbook) 
     If final_NightMins >= 600 And final_DayMins >= 5700 Then
        Debug.Print "Application status: Approved"
        Call WriteToLogFile(logFile, "Application status: Approved")
     Else
        Debug.Print "Application status: " & final_LogbookAppStatus
        Call WriteToLogFile(logFile, "Application status: " & final_LogbookAppStatus)
     End If
     
     Application.ScreenUpdating = True
     Exit Function
Cleanup:
     EvaluateTrips = "Error in Function EvaluateTrips: An unrecoverable error occurred. " & Err.Description
     Debug.Print "An unrecoverable error occurred in Function EvaluateTrips() " & Err.Description
     Application.ScreenUpdating = True
End Function

Function TripValidity(row As Integer, businessRules() As String, brShtRws As Integer) As String
'Accepts the status of each business rule and evaluates them to determine if the trip on the current Row is Valid or Invalid
     
     'Default status of a trip is valid
     TripValidity = "Valid"
     Dim brRow As Integer
     Dim brName As String, brEvaluation As String, brErrorMessage As String
     
     'Loop through the evaluated business rule results and if False, the trip is valid otherwise if True, trip is Invalid
     brRow = 0
     While brRow < brShtRws
          brName = businessRules(brRow, 0)
          brErrorMessage = businessRules(brRow, 6)
          brEvaluation = businessRules(brRow, 7)
          Select Case brEvaluation
               Case "False"
                    TripValidity = "Valid"
               Case "True"
                    TripValidity = brErrorMessage
                    Exit Function
          End Select
          brRow = brRow + 1
     Wend
End Function

Function ProcessTripRow(row As Integer, tripStatus As String, logFile As Object)
'Updates the trip with the relevant error message if the business rule evaluated to true, else clears 
     If tripStatus = "Valid" Then
          Debug.Print "Trip " & row - 1 & " - " & tripStatus
          Call WriteToLogFile(logFile, "Trip " & row - 1 & " - " & tripStatus)
          Worksheets("Data").Unprotect
          Worksheets("Data").Range("U" & row) = ""
          Worksheets("Data").Protect
     Else
          Debug.Print "Trip " & row - 1 & " - " & tripStatus
          Call WriteToLogFile(logFile, "Trip " & row - 1 & " - " & tripStatus)
          Worksheets("Data").Unprotect
          Worksheets("Data").Range("U" & row) = tripStatus
          Worksheets("Data").Protect
     End If
End Function

Function CreateLogFile(crn As String) As Object

     Dim fso As Object
     Set fso = CreateObject("Scripting.FileSystemObject")
     Set CreateLogFile = fso.CreateTextFile("C:\SharedData\UiPath\Learner Logbook\VBA\AssessmentLogs\" & crn & " assessment log file.txt")

End Function

Function WriteToLogFile(logFile As Object, str As String)
    logFile.Write str & vbNewLine
End Function

Function Evaluate_TenHourTrip(tripDuration As String, startLocation As String, finishLocation As String, brTolerance As String, brErrorMessage As String, logFile As Object) As Boolean
'Do something

     Debug.Print "TenHourTrip executing..."
     Call UpdateProgressStatusText("checking long distance trip...")

     'If trip duration <= 600, don't evaluate, trip is OK
     If tripDuration <= 600 Then
          Evaluate_TenHourTrip = False
          Debug.Print "TenHourTrip evaluated to FALSE... (" & tripDuration & " mins vs 600 mins)"
          Call WriteToLogFile(logFile, "TenHourTrip evaluated to FALSE... (" & tripDuration & " mins vs 600 mins)")
          Exit Function
     End If
     
     'If the start and finish locations are not the same, checking travel times on Google
     If Not startLocation = finishLocation Then
          Dim actualTripTime As String
          actualTripTime = GetGoogleTripMinutes(startLocation, finishLocation)
          If Left(actualTripTime, 5) = "Error" Or actualTripTime = "" Then
               Evaluate_TenHourTrip = True
               Debug.Print "InvalidTenHourTrip evaluated to TRUE... " & "(Unable to validate trip on Google maps)"
               Call WriteToLogFile(logFile, "InvalidTenHourTrip evaluated to TRUE... " & "(Unable to validate trip on Google maps)")
               Exit Function
          Else
               'If the actual trip time is within tolerance of the recorded trip time, trip is valid
               If tripDuration <= (actualTripTime * brTolerance + actualTripTime) And _
                  tripDuration >= (actualTripTime - actualTripTime * brTolerance) Then
                    Evaluate_TenHourTrip = False
                    Debug.Print "InvalidTenHourTrip evaluated to FALSE... " & "(" & tripDuration & " vs " & actualTripTime & " / " & brTolerance & ")"
                    Call WriteToLogFile(logFile, "InvalidTenHourTrip evaluated to FALSE... " & "(" & tripDuration & " vs " & actualTripTime & " / " & brTolerance & ")")
                    Exit Function
               Else
                    'Otherwise, the trip is invalid
                    Evaluate_TenHourTrip = True
                    Debug.Print "InvalidTenHourTrip evaluated to TRUE... " & "(" & brErrorMessage & ")"
                    Call WriteToLogFile(logFile, "InvalidTenHourTrip evaluated to TRUE... " & "(" & brErrorMessage & ")")
                    Exit Function
               End If
          End If
     End If

End Function

Function Evaluate_LocationOdometerMismatch(macroResult As String, suburb1 As String, suburb2 As String, odometer1 As String, odometer2 As String, brErrorMessage As String, brTolerance As String, startLocation As String, prevTripStartSuburb As String, prevTripEndSuburb As String, distanceBetweenPrevLocations As String, logFile As Object) As Boolean
'Do something

     Debug.Print "LocationOdometerMismatch executing..."
     Call UpdateProgressStatusText("evaluating location odometer mismatch...")

     'If the existing spreadsheet macro hasn't identified an issue, trip is OK
     If macroResult = "" Then
          Evaluate_LocationOdometerMismatch = False
          Debug.Print "LocationOdometerMismatch evaluated to FALSE... (No indicator from existing macro)"
          Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to FALSE... (No indicator from existing macro)")
          Exit Function
     End If

     'Check if the suburb and odometers match with spaces removed, if so trip OK
     If suburb1 = suburb2 And CLng(odometer1) >= CLng(odometer2) Then
          Evaluate_LocationOdometerMismatch = False
          Debug.Print "LocationOdometerMismatch evaluated to FALSE... Excess spaces in location name"
          Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to FALSE... Excess spaces in location name")
          Exit Function
     End If

     'Check if the odometers are the same
     If CLng(odometer1) = CLng(odometer2) Then
               
          'And if so, get the distance between the two suburbs
          Dim distanceBetweenMismatchedSuburbs As String
          distanceBetweenMismatchedSuburbs = GetGoogleTripDistance(suburb1, suburb2)
                    
          'If there was an error getting the distance, mark as incomplete
          If Left(distanceBetweenMismatchedSuburbs, 5) = "Error" Or distanceBetweenMismatchedSuburbs = "" Or distanceBetweenMismatchedSuburbs = "Unable to find location" Then
               Evaluate_LocationOdometerMismatch = True
               Debug.Print "LocationOdometerMismatch evaluated to TRUE... " & " (unable to verify locations on Google)"
               Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to TRUE... " & " (unable to verify locations on Google)")
               Exit Function
                    
          Else
               If distanceBetweenMismatchedSuburbs = "Spelling Error" Then
                    Evaluate_LocationOdometerMismatch = False
                    Debug.Print "LocationOdometerMismatch evaluated to FALSE... " & "(Simple spelling mistake)"
                    Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to FALSE... " & "(Simple spelling mistake)")
                    Exit Function
               Else

                    'Check if previous trip looks like a return trip
                    If startLocation = prevTripStartSuburb Then
                         Dim likelyReturnTrip As Boolean
                         'If this distance
                         '##########################################################################
                         'Need to work on this formula.............
                         If distanceBetweenPrevLocations = "" Or distanceBetweenPrevLocations = "Unable to find location" Or Left(distanceBetweenPrevLocations, 5) = "Error" Then
                              Evaluate_LocationOdometerMismatch = True
                              Debug.Print "LocationOdometerMismatch evaluated to TRUE... " & " (unable to verify locations on Google)"
                              Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to TRUE... " & " (unable to verify locations on Google)")
                              Exit Function
                         End If
                         If distanceBetweenPrevLocations >= ((distanceBetweenMismatchedSuburbs * 2) * 0.85) Then
                              Evaluate_LocationOdometerMismatch = False
                              Debug.Print "LocationOdometerMismatch evaluated to FALSE... " & "(Looks like a return trip)"
                              Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to FALSE... " & "(Looks like a return trip)")
                              Exit Function
                         End If
                              
                         '##############################################################################
                         'Un/Comment the below if you want to enable or disable this functionality
                         'Enables a trip to be made Valid if the mismatched suburbs are close enough
                         'If the distance was retrieved, and is less than the BR criteria, trip is OK
                         If distanceBetweenMismatchedSuburbs <= CInt(brTolerance) Then
                              Evaluate_LocationOdometerMismatch = False
                              Debug.Print "LocationOdometerMismatch evaluated to FALSE... " & suburb2 & " and " & suburb1 & " are within " & brTolerance & " km of each other"
                              Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to FALSE... " & suburb2 & " and " & suburb1 & " are within " & brTolerance & " km of each other")
                              Exit Function
                         Else
                              'Otherwise, the trip is not OK
                              Evaluate_LocationOdometerMismatch = True
                              Debug.Print "LocationOdometerMismatch evaluated to TRUE... " & "(" & brErrorMessage & ")"
                              Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to TRUE... " & "(" & brErrorMessage & ")")
                              Exit Function
                         End If
                         'Un/Comment the above if you want to enable or disable this functionality
                         '##########################################################################
                    Else
                         'Otherwise, the trip is not OK
                         Evaluate_LocationOdometerMismatch = True
                         Debug.Print "LocationOdometerMismatch evaluated to TRUE... " & "(" & brErrorMessage & ")"
                         Call WriteToLogFile(logFile, "LocationOdometerMismatch evaluated to TRUE... " & "(" & brErrorMessage & ")")
                    End If
               End If
          End If
     End If

End Function

Function Evaluate_IncorrectOdometer(startOdo As String, finishOdo As String, brCriteria As String, brErrorMessage As String, logFile As Object) As Boolean
'Do something
     Debug.Print "IncorrectOdometer executing..."
     Call UpdateProgressStatusText("checking odometer recordings...")

     If startOdo > finishOdo Then
          Evaluate_IncorrectOdometer = True
          Debug.Print "IncorrectOdometer evaluated to TRUE... (Starting odometer is > finishing odometer)..."
          Call WriteToLogFile(logFile, "IncorrectOdometer evaluated to TRUE... (Starting odometer is > finishing odometer)...")
          Exit Function
     End If

     If finishOdo > (startOdo + brCriteria) Then
          Evaluate_IncorrectOdometer = True
          Debug.Print "IncorrectOdometer evaluated to TRUE... (> 100,000km in one trip)..."
          Call WriteToLogFile(logFile, "IncorrectOdometer evaluated to TRUE... (> 100,000km in one trip)...")
          Exit Function
     End If

     Debug.Print "IncorrectOdometer evaluated to FALSE... (No issues identified)..."
     Call WriteToLogFile(logFile, "IncorrectOdometer evaluated to FALSE... (No issues identified)...")
     Evaluate_IncorrectOdometer = False

End Function

Function Evaluate_IncorrectTime(startTime As String, finishTime As String, brErrorMessage As String, logFile As Object) As Boolean
'Do something
     Debug.Print "IncorrectTime executing..."
     Call UpdateProgressStatusText("validating start and finish times...")
     
     Dim startTimeValid As Boolean
     Dim finishTimeValid As Boolean
     startTimeValid = False
     finishTimeValid = False
     
     If IsDate(startTime) = True Then
          If CStr(startTime) Like "*#*.*#*" = False Then
               If Fix(CDate(startTime)) = 0 Then
                    If CStr(startTime) Like "1[3-9]*[aApP]*" = False Then
                         If CStr(startTime) Like "2[0-3]*[aApP]*" = False Then
                              startTimeValid = True
                         End If
                    End If
               End If
          End If
     End If
    
     If IsDate(finishTime) = True Then
          If CStr(finishTime) Like "*#*.*#*" = False Then
               If Fix(CDate(finishTime)) = 0 Then
                    If CStr(finishTime) Like "1[3-9]*[aApP]*" = False Then
                         If CStr(finishTime) Like "2[0-3]*[aApP]*" = False Then
                              finishTimeValid = True
                         End If
                    End If
               End If
          End If
     End If

     If startTimeValid = True And finishTimeValid = True Then
          Evaluate_IncorrectTime = False
          Debug.Print "IncorrectTime evaluated to FALSE... (No issues identified)..."
          Call WriteToLogFile(logFile, "IncorrectTime evaluated to FALSE... (No issues identified)...")
          Exit Function
     Else
          Evaluate_IncorrectTime = True
          Debug.Print "IncorrectTime evaluated to TRUE... (Invalid time recorded)..."
          Call WriteToLogFile(logFile, "IncorrectTime evaluated to TRUE... (Invalid time recorded)...")
     End If
    
End Function

Function Evaluate_InvalidSupervisor(logFile As Object)
'Supervisor check is out of scope for the POC.  Left in for completeness.
     Debug.Print "InvalidSupervisor... (Out of Scope for POC)"
     'Call UpdateProgressStatusText("validating supervisor details...")
End Function

Function Evaluate_InvalidLearner(adjustTripStatus As String, logFile As Object) As Boolean
'Do something
     Debug.Print "InvalidLearner executing..."
     Call UpdateProgressStatusText("checking learner licence details...")
          If adjustTripStatus = "Invalid learner" Then
               Evaluate_InvalidLearner = True
               Debug.Print "InvalidLearner evaluated to TRUE... " & "(Invalid learner)"
               Call WriteToLogFile(logFile, "InvalidLearner evaluated to TRUE... " & "(Invalid learner)")
               Exit Function
          Else
               Evaluate_InvalidLearner = False
               Debug.Print "InvalidLearner evaluated to FALSE... " & "(No issues identified)"
               Call WriteToLogFile(logFile, "InvalidLearner evaluated to FALSE... " & "(No issues identified)")
          End If
     Call UpdateProgressStatusText("validating learner licence periods...")
End Function

Function Evaluate_PriorToLicenceIssueDate(adjustTripStatus As String, brErrorMessage As String, logFile As Object)
'Code to evaluate the licence issue date against the recorded trip date would go here
     Debug.Print "PriorToLicenceIssueDate executing..."
     Call UpdateProgressStatusText("checking licence issue date...")
     If UCase(adjustTripStatus) = UCase("Prior to licence issue") Then
          Evaluate_PriorToLicenceIssueDate = True
          Debug.Print "PriorToLicenceIssueDate evaluated to TRUE... " & "(" & brErrorMessage & ")"
          Call WriteToLogFile(logFile, "PriorToLicenceIssueDate evaluated to TRUE... " & "(" & brErrorMessage & ")")
          Exit Function
     Else
          Evaluate_PriorToLicenceIssueDate = False
          Debug.Print "PriorToLicenceIssueDate evaluated to FALSE... " & "(Trip after licence issue date)"
          Call WriteToLogFile(logFile, "PriorToLicenceIssueDate evaluated to FALSE... " & "(Trip after licence issue date)")
     End If
End Function

Function Evaluate_LowAverageSpeed(estSpeed As String, brOperand As String, brCriteria As String, brTolerance As String, startLocation As String, _
finishLocation As String, brException As String, tripDuration As String, startTime As String, finishTime As String, accreditedMins As String, _
drivingSchool As String, brErrorMessage As String, logFile As Object) As Boolean
     
     Debug.Print "LowAverageSpeed executing..."
     Call UpdateProgressStatusText("determining low average speed...")
     
     brException = Replace(brException, "startTime", startTime)
     brException = Replace(brException, "finishTime", finishTime)
     Dim splitTemp() As String
     splitTemp = Split(brException, " or ", -1)
     Dim morningTimes() As String
     Dim eveningTimes() As String
     morningTimes = Split(splitTemp(0), " - ", -1)
     eveningTimes = Split(splitTemp(1), " - ", -1)
     Dim Speed As Long
     If estSpeed = "0" Or estSpeed = "" Then
          Speed = 0
     Else
          Speed = Round(estSpeed, 0)
     End If

     'Check if speed is above criteria, or it's a driving school trip
     If Speed > Clng(brCriteria) Then
          Evaluate_LowAverageSpeed = False
          Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h is above " & brCriteria & " km/h)"
          Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h is above " & brCriteria & " km/h)")
          Exit Function
     End If
     
     'Check if speed is above criteria, or it's a driving school trip
     If CLng(accreditedMins) > 0 Or drivingSchool <> "" Then
          Evaluate_LowAverageSpeed = False
          Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to Driving School Trip)"
          Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to Driving School Trip)")
          Exit Function
     End If
     
     'Check if trip is within exception times
     If Eval(startTime > morningTimes(0) And finishTime < morningTimes(1) Or _
             startTime > eveningTimes(0) And finishTime < eveningTimes(1)) = True Then
          Evaluate_LowAverageSpeed = False
          Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to trip between exception hours)"
          Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to trip between exception hours)")
          Exit Function
     End If
     
      'Check if trip < 60 mins
     If CLng(tripDuration) < 60 = True Then
          Evaluate_LowAverageSpeed = False
          Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to short trip < 60 mins)"
          Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to short trip < 60 mins)")
          Exit Function
     End If
     
     'Else check if trip is within tolerance of Google estimated trip time
     If Not startLocation = finishLocation Then
          Dim actualTripTime As String
          actualTripTime = GetGoogleTripMinutes(startLocation, finishLocation)
          If Left(actualTripTime, 5) = "Error" Or actualTripTime = "" Or actualTripTime = "Unable to find location" Then
               Evaluate_LowAverageSpeed = True
               Debug.Print "LowAverageSpeed evaluated to TRUE... " & "(Unable to validate trip on Google maps)"
               Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to TRUE... " & "(Unable to validate trip on Google maps)")
               Exit Function
          End If
          If tripDuration <= (actualTripTime * brTolerance + actualTripTime) And _
             tripDuration >= (actualTripTime - actualTripTime * brTolerance) Then
               Evaluate_LowAverageSpeed = False
               Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(Trip within " & Format(brTolerance, "0%") & " of Google estimated time)"
               Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(Trip within " & Format(brTolerance, "0%") & " of Google estimated time)")
               Exit Function
          Else
               If Not tripDuration <= (actualTripTime * brTolerance + actualTripTime) And _
                      tripDuration >= (actualTripTime - actualTripTime * brTolerance) Then
                    Evaluate_LowAverageSpeed = True
                    Debug.Print "LowAverageSpeed evaluated to FALSE... " & "(Reported trip time of " & tripDuration & " mins exceeds Google estimated time of " & actualTripTime & " mins)"
                    Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to FALSE... " & "(Reported trip time of " & tripDuration & " mins exceeds Google estimated time of " & actualTripTime & " mins)")
                    Exit Function
               Else
                    'If none of the above are true, the trip is invalid
                    Evaluate_LowAverageSpeed = True
                    Debug.Print "LowAverageSpeed evaluated to TRUE... " & "(" & brErrorMessage & ")"
                    Call WriteToLogFile(logFile, "LowAverageSpeed evaluated to TRUE... " & "(" & brErrorMessage & ")")
               End If
          End If
     End If

End Function

Function Evaluate_ExcessiveSpeed(estSpeed As String, operand As String, criteria As String, tolerance As String, brErrorMessage As String, startLocation As String, finishLocation As String, tripDuration As String, accreditedMins As String, drivingSchool As String, logFile As Object) As Boolean
'Check if the speed is in excess of 120km/h
     
     Debug.Print "ExcessiveSpeed executing..."
     
     'Update progress bar status text
     Call UpdateProgressStatusText("evaluating excessive speed...")

     'There are scenarios where the estimated speed will be 0
     Dim Speed As Long
     Dim SpeedLimit As Long
     If estSpeed = "0" Or estSpeed = "" Then
          Speed = 0
     Else
          Speed = CLng(Round(estSpeed, 0))
     End If
     SpeedLimit = CLng(criteria)

     'Check if recorded speed is below the excess speed limit criteria
     If Not Eval(Speed - (tolerance * Speed) & operand & SpeedLimit) Then
          Evaluate_ExcessiveSpeed = False
          Debug.Print "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h below criteria of " & speedLimit & " km/h)"
          Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h below criteria of " & speedLimit & " km/h)")
          Exit Function
     End If

     'Check if speed is above criteria, or it's a driving school trip
     If CLng(accreditedMins) > 0 Or drivingSchool <> "" Then
          Evaluate_ExcessiveSpeed = False
          Debug.Print "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to Driving School Trip)"
          Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h ignored due to Driving School Trip)")
     Else
          Evaluate_ExcessiveSpeed = True
          Debug.Print "ExcessiveSpeed evaluated to TRUE... " & "(" & Speed & "km/h above criteria of " & speedLimit " km/h)"
          Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to TRUE... " & "(" & Speed & "km/h above criteria of " & speedLimit & " km/h)")
     End If

     '###############################################################################
     'The below code checks excessive speed trips on Google maps to check if the 
     'recorded time travelled is OK compared to the Google maps estimated time.
     'Uncomment the below If statement if you want to enable this functionality. 
     'If the recorded speed exceeds the excess speed criteria, then check Google
     ' If Eval(Speed - (tolerance * Speed) & operand & SpeedLimit) Then
     '      Dim tripEstimate As String
     '      tripEstimate = GetGoogleTripMinutes(startLocation, finishLocation)

     '      'If Google returns an error, invalidate the trip as we can't confirm trip time
     '      If Left(tripEstimate, 5) = "Error" Or tripEstimate = "" Or tripEstimate = "Unable to find location" Then
     '           Evaluate_ExcessiveSpeed = True
     '           Debug.Print "ExcessiveSpeed evaluated to TRUE... " & "(Unable to verify trip with Google)"
     '           Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to TRUE... " & "(Unable to verify trip with Google)")
     '           Exit Function

     '      'Otherwise check if the recorded trip was within 30% of the Google trip time
     '      Else
     '           If tripDuration <= (tripEstimate * 0.3 + tripEstimate) And _
     '              tripDuration >= (tripEstimate - tripEstimate * 0.3) Then
     '                Evaluate_ExcessiveSpeed = False
     '                Debug.Print "ExcessiveSpeed evaluated to FALSE... " & "(Trip duration within 30% of Google maps time)"
     '                Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to FALSE... " & "(Trip duration within 30% of Google maps time)")
     '                Exit Function
     '           Else
     '                Evaluate_ExcessiveSpeed = True
     '                Debug.Print "ExcessiveSpeed evaluated to TRUE... " & "(Trip not within 30% of Google maps time)"
     '                Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to TRUE... " & "(Trip not within 30% of Google maps time)")
     '                Exit Function
     '           End If
     '      End If
            
     '      'Otherwise, the speed is OK
     '      Else
     '           Evaluate_ExcessiveSpeed = False
     '           Debug.Print "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h below criteria of " & SpeedLimit & "km/h)"
     '           Call WriteToLogFile(logFile, "ExcessiveSpeed evaluated to FALSE... " & "(" & Speed & "km/h below criteria of " & SpeedLimit & "km/h)")
     '  End If
     
End Function

Function GetNumeric(inputString As String) As Integer
'inputString: The string you want to convert to numeric characters only
'Accepts are string as input and returns the string with numeric characters only
     Dim stringLength As Integer
     Dim i As Integer
     stringLength = Len(inputString)
     Dim result As Integer
     For i = 1 To stringLength
          If IsNumeric(Mid(inputString, i, 1)) Then result = result & Mid(inputString, i, 1)
     Next i
     GetNumeric = result
End Function

Function Eval(ref As String)
'Converts a string into an arithmetic operation
     Application.Volatile
     Eval = Evaluate(ref)
End Function

Function DisplayProgressBar()
     On Error Resume Next
     With ProgressBar
          .Show vbModeless
     End With
End Function

Function UpdateProgressBar(currentX As Integer, totalX As Integer)
     On Error Resume Next
     Dim pctCompl As Integer
     pctCompl = (currentX / totalX) * 100
     ProgressBar.Text.Caption = "Trip " & currentX & " of " & totalX & "... (" & Format(pctCompl, "0") & "%)"
     ProgressBar.Bar.Width = pctCompl * 2
     DoEvents
End Function

Function UpdateProgressStatusText(str As String)
     On Error Resume Next
     ProgressBar.Label2.Caption = str
     ProgressBar.Repaint
End Function

Function HideProgressBar()
     On Error Resume Next
     With ProgressBar
          .Hide
     End With
End Function

Function testgoogle()

Call GetGoogleTripDistance("The Village Shopping Center, Emerald", "BP Roadhouse, Clermont")

End Function

Function GetGoogleTripMinutes(fromLocation As String, toLocation As String) As String
'Opens an invisible Internet Explorer session, navigates to google maps and checks the distance
'between the fromLocation and toLocation passed to the function
'fromLocation: Start location to get a distance from
'toLocation: Finish location to get a distance to
'Returns the trip time in hours and minutes between the two locations
     
     'If the from and to locations are the same, exit function.  Google won't return any trip data
     If Trim(fromLocation) = Trim(toLocation) Then
          Debug.Print "Cannot check distance if FROM and TO locations are the same"
          Exit Function
     End If
     
     On Error GoTo Cleanup 'Close open browser and return error message
     
     'Create an instance of Internet Explorer
     Dim browser As Object
     Set browser = CreateObject("InternetExplorer.Application")
     browser.Navigate ("http://maps.google.com/maps?saddr=" & fromLocation & ",australia&daddr=" & toLocation & ",australia")
     browser.Visible = False
     
     '==========================================================
     'This is the class name of the HTML element containing the
     'estimated trip details on Google Maps.  This name can be
     'changed if the website code changes in the future.
     Dim elementClassName As String
     elementClassName = "section-directions-trip-description"
     '==========================================================

     'Wait for Internet Explorer to load
     Dim waiting As Integer
     Dim maxWait As Integer
     maxWait = 10
     Do
          Call UpdateProgressStatusText("loading Internet Explorer... (" & waiting & " secs)")
          Debug.Print "Loading Internet Explorer... (" & waiting & " secs)"
          Application.Wait (Now() + TimeValue("00:00:001"))
          DoEvents
          waiting = waiting + 1
     Loop Until browser.ReadyState = READYSTATE_COMPLETE Or waiting = 5

     Dim doc As Object
     Set doc = browser.Document

     'Wait for the trip details element to load
     Dim firstElement As Variant
     Dim locationNotFound As Variant
     waiting = 0
     Do
          Debug.Print "Loading " & fromLocation & " to " & toLocation & " trip summary... (" & waiting & ")"
          Call UpdateProgressStatusText("checking Google maps... (" & waiting & ")")
          Application.Wait (Now() + TimeValue("00:00:001"))
          On Error Resume Next
          firstElement = doc.getElementsByClassName(elementClassName)(0).outerHTML
          errorElement = doc.getElementsByClassName("section-directions-error-primary-text")(0).outerHTML
          locationNotFound = doc.getElementsByClassName("widget-directions-error-suggestions")(0).outerHTML
          If firstElement = Empty And Not locationNotFound = Empty Then
               GetGoogleTripMinutes = "Unable to find location"
               browser.Quit
               Exit Function
          End If
          If Not errorElement = Empty Then
               Dim getString As String, suburb1 As String, suburb2 As String
               Dim splitString() As String
               suburb1 = CStr(errorElement)
               suburb2 = CStr(errorElement)
               splitString = Split(suburb1, ",", 15)
               suburb1 = splitString(2)
               splitString = Split(suburb1, ">", 5)
               suburb1 = splitString(3)
               splitString = Split(suburb2, ",", 15)
               suburb2 = splitString(3)
               splitString = Split(suburb2, ">", 15)
               suburb2 = splitString(6)
               If suburb1 = suburb2 And suburb1 = Trim(fromLocation) Or suburb1 = Trim(toLocation) Then
                    GetGoogleTripMinutes = "Spelling Error"
                    browser.Quit
                    Exit Function
               End If
          End If
          
          DoEvents
          waiting = waiting + 1
     Loop Until Not firstElement = Empty Or waiting = maxWait Or Not errorElement = Empty
     
     Dim elementNumber As Integer
     Dim element As Variant
     Dim pElement As Variant
     elementNumber = 0
     For Each element In doc.getElementsByClassName(elementClassName)
          'Debug.Print element.ClassName
          'pElement = element.parentElement.getElementsByclassname("section-directions-trip-travel-mode-icon transit") works
          pElement = element.parentElement.getelementsbytagname("div")
          'Debug.Print pElement.getattribute("aria-label")
          If Trim(pElement.getattribute("aria-label")) = "Driving" Then
               element = doc.getElementsByClassName(elementClassName)(elementNumber).outerHTML
               Exit For
          Else
          End If
          elementNumber = elementNumber + 1
     Next element
     
     'If the distance element was retrieved, trim the value and return it
     If Not element = Empty Then
          Dim hours As String
          Dim minutes As String
          Dim totalMinutes As String
          getString = CStr(element)
          splitString = Split(getString, ">", 15)
          getString = splitString(5)
          splitString = Split(getString, "</", 5)
          getString = splitString(0)
          splitString = Split(getString, " ", 4)
          
          'Evaluate hours and minutes
          If splitString(1) = "min" Then
               hours = "0"
               minutes = Trim(splitString(0))
          Else
               If splitString(1) = "h" Then
                    hours = Trim(splitString(0))
                    If splitString(3) = "min" Then
                         minutes = Trim(splitString(2))
                    Else
                         minutes = "0"
                    End If
               End If
          End If
            
          If hours = "" Then
               hours = 0
          End If
          If minutes = "" Then
               minutes = 0
          End If
          
          totalMinutes = (hours * 60) + minutes
          
          'Debug.Print "Estimated trip time: " & totalMinutes
          GetGoogleTripMinutes = totalMinutes
     'Otherwise, return an error
     Else
          Debug.Print "Unable to retrieve data from Google"
          GetGoogleTripMinutes = "Error trimming the value from Google: " & Err.Description
     End If
     browser.Quit
     Exit Function
Cleanup:
     Debug.Print "Error loading Google maps: " & Err.Description
     On Error Resume Next
     browser.Quit
End Function

Function GetGoogleTripDistance(fromLocation As String, toLocation As String) As String
'Opens an invisible Internet Explorer session, navigates to google maps and checks the distance
'between the fromLocation and toLocation passed to the function
'fromLocation: Start location to get a distance from
'toLocation: Finish location to get a distance to
'Returns distance in km between the two locations
'If the from and to locations are the same, exit function.  Google won't return any trip data
     
     If Trim(fromLocation) = Trim(toLocation) Then
          Debug.Print "Cannot check distance if FROM and TO locations are the same"
          Exit Function
     End If
     
     On Error GoTo Cleanup 'Close open browser and return error message
     
     'Create an instance of Internet Explorer
     Dim browser As Object
     Set browser = CreateObject("InternetExplorer.Application")
     browser.Navigate ("http://maps.google.com/maps?saddr=" & fromLocation & ",australia&daddr=" & toLocation & ",australia")
     browser.Visible = False
     
     '==========================================================
     'This is the class name of the HTML element containing the
     'estimated trip details on Google Maps.  This name can be
     'changed if the website code changes in the future.
     Dim elementClassName As String
     elementClassName = "section-directions-trip-description"
     '==========================================================

     Dim errorElement As Variant
     
     'Wait for Internet Explorer to load
     Dim waiting As Integer
     Dim maxWait As Integer
     maxWait = 10
     Do
          Call UpdateProgressStatusText("loading Internet Explorer... (" & waiting & " secs)")
          Debug.Print "Loading Internet Explorer... (" & waiting & " secs)"
          Application.Wait (Now() + TimeValue("00:00:001"))
          DoEvents
          waiting = waiting + 1
     Loop Until browser.ReadyState = READYSTATE_COMPLETE Or waiting = 5

     Dim doc As Object
     Set doc = browser.Document

     'Wait for the trip details element to load
     Dim firstElement As Variant
     Dim locationNotFound As Variant
     waiting = 0
     errorElement = Empty
     Do
          Debug.Print "Loading " & fromLocation & " to " & toLocation & " trip summary... (" & waiting & ")"
          Call UpdateProgressStatusText("checking Google maps... (" & waiting & " secs)")
          Application.Wait (Now() + TimeValue("00:00:001"))
          On Error Resume Next
          firstElement = doc.getElementsByClassName(elementClassName)(0).outerHTML
          errorElement = doc.getElementsByClassName("section-directions-error-primary-text")(0).outerHTML
          locationNotFound = doc.getElementsByClassName("widget-directions-error-suggestions")(0).outerHTML
          If firstElement = Empty And Not locationNotFound = Empty Then
            GetGoogleTripDistance = "Unable to find location"
            browser.Quit
            Exit Function
          End If
          If Not errorElement = Empty Then
               Dim getString As String, suburb1 As String, suburb2 As String
               Dim splitString() As String
               splitString = Split(errorElement, ">", 15)
               suburb1 = splitString(5)
               splitString = Split(suburb1, ">", 15)
               suburb1 = UCase(splitString(0))
               splitString = Split(errorElement, ">", 15)
               suburb2 = UCase(splitString(11))
               'splitString = Split(suburb2, ">", 15)
               'suburb2 = UCase(splitString(6))
               If suburb1 = suburb2 Then 'And suburb1 = Trim(fromLocation) Or suburb1 = Trim(toLocation) Then
                    GetGoogleTripDistance = "Spelling Error"
                    browser.Quit
                    Exit Function
               End If
          End If
          
          DoEvents
          waiting = waiting + 1
     Loop Until Not firstElement = Empty Or waiting = maxWait Or Not errorElement = Empty
     
     Dim elementNumber As Integer
     Dim element As Variant
     Dim pElement As Variant
     elementNumber = 0
     For Each element In doc.getElementsByClassName(elementClassName)
          'Debug.Print element.ClassName
          'pElement = element.parentElement.getElementsByclassname("section-directions-trip-travel-mode-icon transit") works
          pElement = element.parentElement.getelementsbytagname("div")
          'Debug.Print pElement.getattribute("aria-label")
          If Trim(pElement.getattribute("aria-label")) = "Driving" Or Trim(pElement.getattribute("aria-label")) = "Walking" Then
               element = doc.getElementsByClassName(elementClassName)(elementNumber).outerHTML
               Exit For
          Else
          End If
          elementNumber = elementNumber + 1
     Next element

     'If the distance element was retrieved, trim the value and return it
     If Not element = Empty Then
          Dim distance As String
          getString = CStr(element)
          splitString = Split(getString, "m</div>", 2)
          getString = splitString(0)
          splitString = Split(getString, ">", -1)
          Dim arrayLength As Integer
          arrayLength = arrayLen(splitString)
          distance = Trim(splitString(arrayLength - 1))
          If Right(distance, 1) = "k" Then
               distance = Replace(distance, " k", "")
               Else
               distance = distance / 1000
          End If
          Debug.Print "Distance: " & distance
          GetGoogleTripDistance = distance
     
     'Otherwise, return an error
     Else
          Debug.Print "Unable to retrieve data from Google"
          GetGoogleTripDistance = "Error trimming the value from Google: " & Err.Description
     End If
     browser.Quit
     Exit Function
Cleanup:
     Debug.Print "Error loading Google maps: " & Err.Description
     browser.Quit
End Function

Function arrayLen(arr As Variant) As Integer
'Returns the length of the array passed to it
     arrayLen = UBound(arr) - LBound(arr) + 1
End Function

Function AlphaNumericOnly(inputString As String) As String
'inputString: The string you want to convert to alpha numeric characters only
'Accepts are string as input and returns the string with alpha numeric, '&()' and space characters only
     Dim i As Integer
     Dim strResult As String
     For i = 1 To Len(inputString)
          Select Case Asc(Mid(inputString, i, 1))
               Case 32, 38, 47, 48 To 57, 65 To 90, 97 To 122:  'include 32 if you want to include space
                    strResult = strResult & Mid(inputString, i, 1)
          End Select
     Next
     AlphaNumericOnly = strResult
End Function

Sub ReplaceMsgBoxInCodeModule()
'Adds the VBE reference library (if required) and replaces specific MsgBox text code with Nothing
'The purpose of this is to stop msgboxes from appearing which would interrupt code execution
     On Error Resume Next
     Call AddLib("VBIDE", "{0002E157-0000-0000-C000-000000000046}", 5, 3)
     Call ReplaceTextInCodeModules("MsgBox " & Chr(34) & "Please Close LLB APP Assessment Log Workbook before recording assessment result" & Chr(34), "'Do nothing")
End Sub

Private Function AddLib(libName As String, guid As String, major As Long, minor As Long)
'Imports the VBE reference library
     Dim excelApp As Object
     Set excelApp = Excel.Application
     Dim vbProj As Object
     Set vbProj = excelApp.ActiveWorkbook.VBProject
     Dim chkRef As Object
     For Each chkRef In vbProj.References
          If chkRef.Name = libName Then
               GoTo CleanUp
          End If
     Next
     vbProj.References.AddFromGuid guid, major, minor
CleanUp:
     Set vbProj = Nothing
End Function

Sub ReplaceTextInCodeModules(findStr As String, replaceStr As String)
'Loops through each VBA module in the workbook and replaces the findStr with the replaceStr if found
'This is only used to remove msgboxes from the code
     On Error Resume Next
     Dim mainWB As Workbook
     Dim vbProj As VBIDE.VBProject
     Dim VBComp As VBIDE.VBComponent
     Dim CodeMod As VBIDE.CodeModule
     Dim numLines As Long
     Dim lineNum As Long
     Dim thisLine As String
     Dim message As String
     Dim numFound As Long
     numFound = 0
     Set mainWB = ActiveWorkbook
     Set vbProj = mainWB.VBProject
     For Each VBComp In vbProj.VBComponents
          'Set VBComp = vbProj.VBComponents("ExportStats")
          Set CodeMod = VBComp.CodeModule
          With CodeMod
               numLines = .CountOfLines
               For lineNum = 1 To numLines
                    thisLine = .Lines(lineNum, 1)
                    If InStr(1, thisLine, findStr, vbTextCompare) > 0 Then
                         message = message & mainWB.Name & " | " & VBComp.Name & " | Line #" & lineNum & vbNewLine
                         .ReplaceLine lineNum, Replace(thisLine, findStr, replaceStr, , , vbTextCompare)
                         numFound = numFound + 1
                    End If
               Next lineNum
          End With
     Next VBComp
     Debug.Print "Found: " & numFound
     If message <> "" Then
          Debug.Print message
     End If
End Sub

Function UpdateInvalidDatesTake2()
    Dim startDate As Date
    Dim endDate As Date
    Dim checkdate As Date
    Dim rowCount As Integer
    Dim invalidRowCount As Integer
    Dim supName As String
    
    Sheets("Data").Unprotect
    
    rowCount = WorksheetFunction.CountA(Worksheets("Data").Range("A:A"))
    'Learner invalid dates
    invalidRowCount = WorksheetFunction.CountA(Worksheets("Invalid Dates").Range("C14:C33")) + 13
    For r = 2 To rowCount ' loop through list of trips
        checkdate = Worksheets("Data").Range("A" & r)
        For chkrow = 14 To 33
            startDate = Worksheets("Invalid Dates").Range("C" & chkrow)
            endDate = Worksheets("Invalid Dates").Range("D" & chkrow)
                If checkdate >= startDate Then
                    If checkdate <= endDate Then
                        Worksheets("Data").Range("U" & r) = "Invalid learner"
                    End If
                End If
        Next chkrow
    Next r
    
    Sheets("Data").Protect
    
End Function
